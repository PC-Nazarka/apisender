import pytest
from rest_framework import test


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def api_client() -> test.APIClient:
    """Create api client."""
    return test.APIClient()
