import os

from celery import Celery, schedules

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
app = Celery("api_sender")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "daily_statistics": {
        "task": "apps.mailing.tasks.daily_statistics",
        "schedule": schedules.crontab(minute="0", hour="0"),
    },
}
