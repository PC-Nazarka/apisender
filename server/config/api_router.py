from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from apps.mailing import views

router = DefaultRouter() if settings.DEBUG else SimpleRouter()

router.register(
    prefix='clients',
    viewset=views.ClientViewSet,
    basename='clients',
)
router.register(
    prefix='mailing',
    viewset=views.MailingViewSet,
    basename='mailing',
)

app_name = "api"
urlpatterns = router.urls
