import pytz
from django.core.validators import RegexValidator
from django.db import models

phone_regex = RegexValidator(
    regex=r'^7\d{9,10}$',
    message=(
        "Телефонный номер должен быть введен в формате: "
        "'7XXXXXXXXXX', где X - числа от 0 до 9"
    ),
)


class Mailing(models.Model):

    class Filters(models.TextChoices):
        MOBILE_PHONE_CODE = "MOBILE_PHONE_CODE", "Код мобильного оператора"
        TAG = "TAG", "Тег"

    datetime_start = models.DateTimeField(
        verbose_name="Дата и время запуска рассылки",
    )
    message = models.TextField(
        verbose_name="Текст сообщения для доставки клиенту",
    )
    filter_method = models.TextField(
        choices=Filters.choices,
        verbose_name="Метод фильтрации клиентов",
    )
    filter_value = models.TextField(
        verbose_name="Значение фильтра",
    )
    datetime_end = models.DateTimeField(
        verbose_name="Дата и время окончания рассылки",
    )


class Client(models.Model):

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(
        max_length=11,
        validators=[phone_regex],
        unique=True,
        verbose_name="Телефонный номер",
    )
    mobile_phone_code = models.TextField(
        verbose_name="Код мобильного оператора",
    )
    tag = models.TextField(
        verbose_name="Тег",
    )
    timezone = models.CharField(
        max_length=100,
        choices=TIMEZONES,
        default='UTC',
        verbose_name="Часовой пояс",
    )


class Message(models.Model):

    datetime_created = models.DateTimeField(
        auto_now_add=True,
        verbose_name="Дата и время создания",
    )
    status = models.IntegerField(
        verbose_name="Статус отправки",
    )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        related_name="messages",
        verbose_name="Рассылка, в рамках которой было отправлено сообщение",
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name="messages",
        verbose_name="Клиент, которому отправили",
    )
