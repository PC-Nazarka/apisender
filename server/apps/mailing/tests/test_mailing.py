import datetime

import pytest
import pytz
from django.urls import reverse_lazy
from rest_framework import status

from apps.mailing.models import Mailing

pytestmark = pytest.mark.django_db


def test_create_mailing(
    api_client,
) -> None:
    response = api_client.post(
        reverse_lazy("api:mailing-list"),
        data={
            "datetime_start": (
                datetime.datetime.now(tz=pytz.UTC) -
                datetime.timedelta(days=2)
            ).strftime('%Y-%m-%dT%H:%M:%S%z'),
            "message": "some message",
            "filter_method": Mailing.Filters.MOBILE_PHONE_CODE,
            "filter_value": "999",
            "datetime_end": (
                datetime.datetime.now(tz=pytz.UTC) +
                datetime.timedelta(days=2)
            ).strftime('%Y-%m-%dT%H:%M:%S%z'),
        },
    )
    assert response.status_code == status.HTTP_201_CREATED


def test_update_mailing(
    api_client,
) -> None:
    data = {
        "datetime_start": (
            datetime.datetime.now(tz=pytz.UTC) -
            datetime.timedelta(days=2)
        ),
        "message": "some message",
        "filter_method": Mailing.Filters.MOBILE_PHONE_CODE,
        "filter_value": "999",
        "datetime_end": (
            datetime.datetime.now() +
            datetime.timedelta(days=2)
        ),
    }
    mailing = Mailing.objects.create(**data)
    message = "New message"
    response = api_client.put(
        reverse_lazy(
            "api:mailing-detail",
            kwargs={"pk": mailing.pk},
        ),
        data={
            "datetime_start": (
                datetime.datetime.now(tz=pytz.UTC) -
                datetime.timedelta(days=2)
            ),
            "message": message,
            "filter_method": Mailing.Filters.MOBILE_PHONE_CODE,
            "filter_value": "999",
            "datetime_end": (
                datetime.datetime.now() +
                datetime.timedelta(days=2)
            ),
        },
    )
    assert response.status_code == status.HTTP_200_OK
    assert Mailing.objects.filter(
        id=mailing.pk,
        message=message,
    ).exists()


def test_remove_mailing(
    api_client,
) -> None:
    data = {
        "datetime_start": (
            datetime.datetime.now(tz=pytz.UTC) -
            datetime.timedelta(days=2)
        ),
        "message": "some message",
        "filter_method": Mailing.Filters.MOBILE_PHONE_CODE,
        "filter_value": "999",
        "datetime_end": (
            datetime.datetime.now() +
            datetime.timedelta(days=2)
        ),
    }
    mailing = Mailing.objects.create(**data)
    mailing_id = mailing.pk
    api_client.delete(
        reverse_lazy(
            "api:mailing-detail",
            kwargs={"pk": mailing_id},
        ),
    )
    assert not Mailing.objects.filter(id=mailing_id).exists()
