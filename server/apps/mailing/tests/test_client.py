import pytest
from django.urls import reverse_lazy
from rest_framework import status

from apps.mailing.models import Client

pytestmark = pytest.mark.django_db


def test_create_client(
    api_client,
) -> None:
    response = api_client.post(
        reverse_lazy("api:clients-list"),
        data={
            "phone_number": "7999999999",
            "mobile_phone_code": "913",
            "tag": "tag",
        },
    )
    assert response.status_code == status.HTTP_201_CREATED


def test_update_client(
    api_client,
) -> None:
    data = {
        "phone_number": "7999999999",
        "mobile_phone_code": "913",
        "tag": "tag",
        "timezone": "Asia/Krasnoyarsk",
    }
    client = Client.objects.create(**data)
    tag = "New tag"
    response = api_client.patch(
        reverse_lazy(
            "api:clients-detail",
            kwargs={"pk": client.pk},
        ),
        data={
            "tag": tag,
        },
    )
    assert response.status_code == status.HTTP_200_OK
    assert Client.objects.filter(
        id=client.pk,
        tag=tag,
    ).exists()


def test_remove_client(
    api_client,
) -> None:
    data = {
        "phone_number": "7999999999",
        "mobile_phone_code": "913",
        "tag": "tag",
        "timezone": "Asia/Krasnoyarsk",
    }
    client = Client.objects.create(**data)
    client_id = client.pk
    api_client.delete(
        reverse_lazy(
            "api:clients-detail",
            kwargs={"pk": client_id},
        ),
    )
    assert not Client.objects.filter(id=client_id).exists()
