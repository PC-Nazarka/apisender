import os

import requests
from rest_framework import status

from config.celery import app

from . import models, serializers, services

URL = "https://probe.fbrq.cloud/v1/send/{}"
TOKEN = os.getenv("TOKEN")
SECONDS = 30


@app.task
def sender(client_id: int, mailing_id: int) -> None:
    """Задача для отправки рассылки."""
    mailing = models.Mailing.objects.get(id=mailing_id)
    client = models.Client.objects.get(id=client_id)
    response = requests.post(
        url=URL.format(mailing_id),
        json={
            "id": mailing_id,
            "phone": int(client.phone_number),
            "text": mailing.message,
        },
        headers={
            "Authorization": f"Bearer {TOKEN}",
        }
    )
    models.Message.objects.create(
        status=response.json()["code"],
        mailing=mailing,
        client=client,
    )
    if response.status_code not in (
        status.HTTP_200_OK,
        status.HTTP_401_UNAUTHORIZED,
    ):
        sender.apply_async(
            args=(client_id, mailing_id),
            countdown=SECONDS,
        )


@app.task
def send_message(mailing_id: int) -> None:
    """Задача для сбора клиентов для отправки рассылки."""
    mailing = models.Mailing.objects.get(id=mailing_id)
    clients = []
    match mailing.filter_method:
        case models.Mailing.Filters.MOBILE_PHONE_CODE:
            clients = models.Client.objects.filter(
                mobile_phone_code=mailing.filter_value,
            )
        case models.Mailing.Filters.TAG:
            clients = models.Client.objects.filter(
                tag=mailing.filter_value,
            )
    for client in clients:
        sender.delay(client.id, mailing_id)


@app.task
def daily_statistics() -> None:
    """Задача для ежедневной отправки статистики."""
    mailing_id = models.Message.objects.select_related(
        "mailing",
    ).values_list("mailing", flat=True)
    mailings = serializers.MailingSerializer(
        models.Mailing.objects.prefetch_related(
            "messages",
        ).filter(id__in=mailing_id),
        many=True,
    ).data
    services.send_email("statistic", mailings)
