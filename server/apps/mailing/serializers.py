from rest_framework import serializers

from . import models


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Client
        fields = (
            "id",
            "phone_number",
            "mobile_phone_code",
            "tag",
            "timezone",
        )


class MessageSerializer(serializers.ModelSerializer):

    client = serializers.PrimaryKeyRelatedField(
        queryset=models.Client.objects.all(),
    )

    class Meta:
        model = models.Message
        fields = (
            "id",
            "datetime_created",
            "status",
            "client",
        )


class MailingSerializer(serializers.ModelSerializer):

    count_messages = serializers.SerializerMethodField(read_only=True)
    messages = MessageSerializer(many=True, read_only=True)

    class Meta:
        model = models.Mailing
        fields = (
            "id",
            "datetime_start",
            "message",
            "filter_method",
            "filter_value",
            "datetime_end",
            "count_messages",
            "messages",
        )

    def validate(self, attrs) -> dict:
        if attrs["datetime_start"] >= attrs["datetime_end"]:
            raise serializers.ValidationError(
                "Время начала не может быть позже времени окончания",
            )
        return attrs

    def get_count_messages(self, obj) -> int:
        """Метод для получения количества сообщений."""
        return obj.messages.count()
