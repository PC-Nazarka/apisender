from datetime import datetime

from django.utils import timezone
from rest_framework import decorators, mixins, response, status, viewsets

from apps.mailing.tasks import send_message

from . import models, serializers

FORMAT = '%Y-%m-%dT%H:%M:%S%z'


class CreateUpdateDestroyViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    pass


class ClientViewSet(CreateUpdateDestroyViewSet):
    """ClientViewSet для создания, обновления, удаления клиентов."""

    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer


class MailingViewSet(CreateUpdateDestroyViewSet):
    """
    MailingViewSet для создания, обновления, удаления рассылки
    и для просмотра статистики.
    """

    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer

    def perform_create(self, serializer) -> None:
        """Переопределенный метод для сохранения и отправки рассылки."""
        serializer.save()
        datetime_start = datetime.strptime(
            serializer["datetime_start"].value,
            FORMAT,
        )
        datetime_end = datetime.strptime(
            serializer["datetime_end"].value,
            FORMAT,
        )
        if datetime_start <= timezone.now() <= datetime_end:
            send_message.delay(serializer["id"].value)
        elif timezone.now() < datetime_start:
            send_message.apply_async(
                (serializer["id"].value,),
                countdown=(datetime_start - timezone.now()).total_seconds(),
            )

    @decorators.action(
        methods=("GET",),
        detail=False,
        url_path='total-statistic',
    )
    def total_statistic(self, request, *args, **kwargs):
        """
        Контроллер для отправки статистики по всем отправленным рассылкам.
        Возвращает список элементов расслыки, количество сообщений у каждого
        и сами сообщения.
        """
        mailing_id = models.Message.objects.select_related(
            "mailing",
        ).values_list("mailing", flat=True)
        return response.Response(
            data=serializers.MailingSerializer(
                models.Mailing.objects.prefetch_related(
                    "messages",
                ).filter(id__in=mailing_id),
                many=True,
            ).data,
            status=status.HTTP_200_OK,
        )

    @decorators.action(
        methods=("GET",),
        detail=True,
        url_path='statistic-detail',
    )
    def statistic_detail(self, request, pk, *args, **kwargs):
        """
        Контроллер для отправки статистики по конкретной рассылке.
        Возвращает элемент расслыки, количество сообщений и сами сообщения.
        """
        return response.Response(
            data=serializers.MailingSerializer(
                models.Mailing.objects.prefetch_related(
                    "messages",
                ).get(id=pk),
            ).data,
            status=status.HTTP_200_OK,
        )
