import os

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

TEMPLATES = {
    "statistic": "statistic/statistic.html",
}


def send_email(action, mailings):
    """Функция для отправки email."""
    context = {
        "mailings": mailings,
    }
    html = get_template(TEMPLATES[action])
    msg = EmailMultiAlternatives(
        from_email=settings.EMAIL_HOST_USER,
        to=(os.getenv("DJANGO_EMAIL_TO_STATISTIC"),),
    )
    msg.attach_alternative(html.render(context), "text/html")
    msg.send()
