from django.contrib import admin

from . import models


@admin.register(models.Mailing)
class MailingAdmin(admin.ModelAdmin):

    list_display = (
        "id",
        "datetime_start",
        "message",
        "filter_method",
        "filter_value",
        "datetime_end",
    )


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):

    list_display = (
        "id",
        "phone_number",
        "mobile_phone_code",
        "tag",
        "timezone",
    )


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):

    list_display = (
        "id",
        "datetime_created",
        "status",
        "mailing",
        "client",
    )
